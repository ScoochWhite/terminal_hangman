import requests
import re
import PySimpleGUI as sg
import time
from datetime import datetime


d_day = datetime.strptime("2019-09-20", "%Y-%m-%d")

def area_51_widget(layout):
    page = requests.get('https://www.facebook.com/events/448435052621047/', verify=False)

    page_text = page.text

    pattern = r'"_5z74"\D*([\d|.]*)(\S) Going&nbsp;·&nbsp;([\d|.]*)(\S)'

    matches = re.findall(pattern,page_text)[0]
    layout[0][0].Update(value=(str(matches[0]) + str(matches[1])+ " are going"))
    layout[1][0].Update(value=(str(matches[2]) + str(matches[3])+ " are interested"))
    layout[2][0].Update(value=("Days to D-Day: "+str(abs((datetime.today() - d_day).days))))
    
layout = [
    [sg.Text("going", size=(25,1))],
    [sg.Text("interested", size=(25,1))],
    [sg.Text("Days to D-Day: ", size=(15,1))],
    [sg.Button("Refresh")]
]
print(layout)


window = sg.Window('Area 51 Widget',layout)
 
window.Finalize()
area_51_widget(layout)

while True:
    event, values = window.Read()
    if event == "Refresh":
        area_51_widget(layout)
    if event == None:
        break
window.Close()
