from bs4 import BeautifulSoup
import requests
import csv

site = requests.get("https://www.ksl.com")

soup = BeautifulSoup(site.text, 'lxml')
story = soup.find_all('div', class_='queue_story')

with open('ksl_headlines.csv','w', newline = '') as ksl_headlines:
        csv_writer = csv.writer(ksl_headlines, delimiter=',')
        csv_writer.writerow(['Title', 'Link', 'Author', 'Date', 'Summary'])
        for i in story:
                article = i.text.split('|')
                article_num = str(i.a).split('/')[2]
                if article_num == 'www.ksl.com':
                        link = article_num
                else:
                        link = f"https://www.ksl.com/article/{article_num}"
                title = i.find('h2').text
                author = i.find('span',class_='long').text
                date = i.find('span',class_='short').text
                summary = i.find('h5').text
                csv_writer.writerow([title, link, author, date, summary])

ksl_headlines.close()