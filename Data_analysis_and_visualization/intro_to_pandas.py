import pandas as pd
import numpy as np
from pandas import Series, DataFrame

'''Lecture 14 = Series'''

obj = Series([3,6,9,12])

print(obj)
print(obj.values)
print(obj.index)

ww2_cas = Series([8700000, 4300000, 3000000, 2100000, 400000], index = ['USSR', 'Germany', 'China', 'Japan', 'USA'])

print(ww2_cas)
print(ww2_cas['USA'])

#check which coutries has casualties greater than 4 million
print(ww2_cas[ww2_cas > 4000000])

print('USSR' in ww2_cas)



