import numpy as np

'''Lecture 7 - Creating arrays'''

# my_list1 = [1,2,3,4]

# myarray1 = np.array(my_list1)

# my_list2 = [11,22,33,44]

# my_lists = [my_list1,my_list2]

# myarray2 = np.array(my_lists)

# print(myarray2.shape)

# print(np.arange(5))


'''Lecture 8 - Using arrays and scalars'''

# print(5/2)

# arr1 = np.array([[1,2,3,4],[8,9,10,11]])
# print(arr1)

# print(arr1*arr1)

# print(arr1-arr1)

# print(1/arr1)

# print(arr1**3)

'''Lecture 9 - Indexing Arrays'''

# arr = np.arange(0,11)

# print(arr)

# print(arr[8])

# print(arr[1:5])

# print(arr[0:5])

# arr[0:5] = 100

# print(arr)

# arr = np.arange(0,11)

# print(arr)

# slice_of_array = arr[0:6]

# print(slice_of_array)

# slice_of_array[:] = 99

# print(slice_of_array)

# print(arr)

# arr_copy = arr.copy()

# print(arr_copy)

# arr_2d = np.array([[5,10,15],[20,25,30],[35,40,45]])

# print(arr_2d[1])

# print(arr_2d[0])

# print(arr_2d[1][0])

# print(arr_2d)

# print(arr_2d[:2,1:])

# print(arr_2d[2])

# arr2d = np.zeros((10,10))

# print(arr2d)

# arr_length = arr2d.shape[1]

# print(arr_length)

# for i in range(arr_length):
#     arr2d[i]=1

# print(arr2d)

# for i in range(arr_length):
#     arr2d[i] = i

# print(arr2d)

# print(arr2d[[2,4,6,8]])


'''Lecture 10 - Array transposition'''

# arr = np.arange(50).reshape(10,5)

# print(arr)
# print(arr.T)
# print(np.dot(arr.T,arr))

# arr = np.array([[1,2,3]])
# print(arr)
# print(arr.swapaxes(0,1))

'''Lecture 11 - Universal Array Function'''

# arr = np.arange(11)

# print(arr)

# print(np.sqrt(arr))

# print(np.exp(arr))

# a = np.random.randn(10)
# b = np.random.randn(10)

# print(np.add(a,b))

# print(np.maximum(a,b))

''' Lecture 12 - Array Processing'''

# import numpy as np
# import matplotlib.pyplot as plt

# points = np.arange(-5,5,0.01)

# dx,dy = np.meshgrid(points, points)

# print(dx)

# print(dy)

# z = (np.sin(dx) + np.sin(dy))
# print(z)

# plt.imshow(z)
# plt.colorbar()

# plt.title('Plot for sin(x) + sin(y)')

# A = np.array([1,2,3,4])
# B = np.array([100,200,300,400])
# condition = np.array([True, True, False, False])
# answer = [(a_val if cond else b_val) for a_val, b_val, cond in zip(A,B,condition)]
# print(answer)

# answer2 = np.where(condition,A,B)
# print(answer2)

# arr = np.random.randn(5,5)

# print(arr)

# print(np.where(arr<0,0,arr))

'''Lecture 13 - Array Input and Output'''

arr = np.arange(5)
# store as a npy file
np.save('myarray',arr)

arr = np.arange(10)

np.load('myarray.npy')

