import PySimpleGUI as sg
import csv
from datetime import datetime as dt 
from os import system

def read_csv(filename):
    with open(filename, 'r', encoding='utf-8-sig') as csvfile:
        return [row for row in csv.reader(csvfile)]
        

def write_to_csv(filename, data):
    with open(filename, 'w+') as csvfile:
        writer = csv.writer(csvfile)
        for row in data:
            writer.writerow(row) 

def add_customer(customer_list):
    layout = [[sg.Text('Customer',size=(15,1)),sg.InputText(size=(15,1), key='_new_customer_')],
              [sg.Button('Submit'),sg.Button('Cancel')]]
    window = sg.Window('Add Customer', layout)
    event, values = window.Read()
    while True:
        if event == 'Submit':
            if values['_new_customer_'] != '':
                customer_list.append(values['_new_customer_'])
            else:
                break
        if event == 'Cancel' or event == None:
            break
    window.Close()

def add_item():
    pass

def build_inventory_dict(csvobject):
    header = []
    inventory = {}
    for i, row in enumerate(csvobject):
        for j, element in enumerate(row):
            if i == 0:
                inventory[element] = []
                header.append(element)
            else:
                inventory[header[j]].append(element)
    return inventory

def row_at(idx, dictionary):
    row = {}
    for header in dictionary.keys():
        column = dictionary[header]
        row[header] = column[idx] 
    return row

def list_builder(lst, dictionary, header):
    for i in dictionary[header]:
        lst.append(i)

def add_to_inventory():
    inventory = build_inventory_dict(read_csv('Inventory.csv'))
    customer_list = []
    items_list = []
    list_builder(customer_list, inventory, 'Customer')
    list_builder(items_list, inventory, 'Item')
    layout = [[sg.Text('Customer', size=(8,1)),sg.Drop(customer_list,size=(15,1),key='_Customer_')],
              [sg.Text('Item',size=(8,1)),sg.Drop(items_list,size=(15,1),key=('_Item_'))],
              [sg.Text('Quantity',size=(8,1)),sg.Input(size=(15,1),key='_Quantity_')],
              [sg.Button('Submit'),sg.Button('Cancel')]]
    while True:
        window = sg.Window('Add to Inventory',layout)
        event, values = window.Read()
        if event == 'Cancel' or event == None:
            break
        if event == 'Submit':
            with open('Inventory.csv','w') as csvfile:
                write = csv.writer(csvfile)
                write.writerow([dt.today().strftime('%x'),values['_Customer_'],values['_Item_'],values['_Quantity_']])
    window.Close()

system('cls')
add_to_inventory()

# masons_purchases = []
# for i in range(len(inventory)):
#     if (row_at(i, inventory)['Customer'] == 'Mason'):
#         masons_purchases.append(row_at(i, inventory))
# print(masons_purchases)



