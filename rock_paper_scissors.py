import random
import os
computer = {1:'Rock', 2:'Paper', 3:'Scissors'}
player = {'r':'Rock', 'p':'Paper', 's':'Scissors', 'q':'Quit'}
scenarios = {'Rock':'Scissors','Paper':'Rock','Scissors':'Paper'}
while True:
    os.system('cls')
    try:
        player_choice = player[input('Rock, paper, scissors SHOOT! ').lower()[0]]
    except:
        continue
    if player_choice == 'Quit':
        break
    choice = computer[random.randint(1,3)]
    print('Player:', player_choice+'\nComputer:',choice)
    if choice == scenarios[player_choice]:
        print('You won!!!')
    elif choice == player_choice:
        print("It's a tie")
    else:
        print('You lose...')
    play_again = input('\nPlay again? (y/n) ')
    if play_again.lower()[0] != 'y':
        break
