import time
from datetime import datetime
import os

def makeStickman(direction):
    stickman = [['       ',
                 '   O  o',
                 '  /T\/ ',
                 ' / |   ',
                 '   |\  ',
                 '  /  | ',
                 '_______'],
                ['       ',
                 '   O   ',
                 '  /T\_o',
                 ' / |   ',
                 '   |\  ',
                 '  /  | ',
                 '_______'],
                ['       ',
                 '   O   ',
                 '  /T\  ',
                 ' / | \:',
                 '   |\ o',
                 '  /  | ',
                 '_______'],
                ['       ',
                 '   O   ',
                 '  /T\  ',
                 ' / | \ ',
                 '  / \  ',
                 ' /  / o',
                 '_______'],
                ['       ',
                 '   O   ',
                 '  /T\  ',
                 ' / | \ ',
                 '  / \ o',
                 ' /  / :',
                 '_______'],
                ['       ',
                 '   O   ',
                 '  /T\_o',
                 ' / |  :',
                 '  / \  ',
                 ' /  /  ',
                 '_______']]
    if not direction:
        for v in stickman:
            for i,string in enumerate(v):
                v[i] = string[::-1]
                v[i] = v[i].replace('/','a')
                v[i] = v[i].replace('\\','/')
                v[i] = v[i].replace('a','\\')
    return stickman

# directions = {
#     "left": 0,
#     "right": 1,
# }

timevalue = .01
counter = 0
distance = 10

# pixelsPerSecond = float(input('How many pixels per second would you like? '))
# framesPerSecond = float(input('How many frames per second would you like? '))

# direction = directions["right"]
# position = 0

# stickmanRight = makeStickman(True)
# stickmanLeft = makeStickman(False)

# def clamp(value, minValue, maxValue):
#     return min(maxValue, max(minValue, value))

# def walkForward(dt):
#     global position
#     position += (1 if direction == directions["right"] else -1) * pixelsPerSecond * dt
#     position = clamp(position, 0.0, distance)

# def turnAround():
#     global direction
#     direction = 1 - direction

# def isAtEdge():
#     return position >= distance if direction == directions["right"] else position <= 0

# def drawStickMan(frame):
#     animFrame = ""
#     for row in stickmanRight[frame] if direction == directions["right"] else stickmanLeft[frame]:
#         animFrame += ((" "*int(position))+row+"\n")
#     animFrame += ('\n'*5)
#     print(animFrame)

# lastTime = datetime.now()
# while True:
#     currentTime = datetime.now()
#     dt = (currentTime - lastTime).total_seconds()
#     if isAtEdge():
#         turnAround()
#     walkForward(dt)
#     drawStickMan(int(counter))
#     counter += dt * framesPerSecond
#     if counter >= len(stickmanRight):
#         counter -= len(stickmanRight)
#         # time.sleep(0.01666666666667)
#         time.sleep(.1)
#     lastTime = currentTime

while True:
    if counter % distance is 0:
        forward = (counter//distance) % 2 is 0
        stickman = makeStickman(forward)
    for v in stickman:
        time.sleep(timevalue)
        man = ""
        for row in v:
            man += " "*counter + row + '\n'
            # print((" "*counter)+row)
        os.system('cls')
        print(man)
        counter+= 1 if forward else -1
